#include <iostream>
#include <math.h>

struct P
{
    double mass;
    double energy_recharge_rate;
    double max_energy;
    double max_cargo;
    double max_missiles;
    double max_flight_speed;
    double max_flight_pitch;
    double max_flight_roll;
};

double rechne(const P &p)
{
    const double mass_cost = 0.5 * std::max(0.0, p.mass);
    const double cargo_cost = 0.5 * std::max(0.0, 20000.0 * (std::exp(10000.0 * p.max_cargo / (3 * p.mass)) - 1.0));
    const double energy_recharge_rate_cost =
        0.5 * std::max(0.0, 2000.0 * std::exp(20.0 * p.energy_recharge_rate / 17.0) / 3.0 - 630.0);
    const double max_energy_cost = 0.5 * std::max(0.0, 80.0 * (125.0 * std::exp(p.max_energy / 200.0) - 112.0));
    const double max_missiles_cost =
        0.5 *
        std::max(0.0, 5.0 * (1000.0 * std::exp(500 * p.max_missiles / (19 * (std::pow(p.mass, 1.0 / 3.0)))) - 991));
    const double max_flight_speed_cost = 0.5 * std::max(0.0, p.mass * std::exp(p.max_flight_speed / 80.0) / 70.0);
    const double flight_control_cost =
        0.5 * std::max(0.0, p.mass * std::exp(3.0 * (p.max_flight_pitch * 2.0 + p.max_flight_roll) / 4.0) / 80.0);

    std::cout << "mass: " << p.mass << ", mass_cost: " << mass_cost << std::endl;
    std::cout << "max_cargo: " << p.max_cargo << ", cargo_cost: " << cargo_cost << std::endl;
    std::cout << "energy_recharge_rate: " << p.energy_recharge_rate
              << ", energy_recharge_rate_cost: " << energy_recharge_rate_cost << std::endl;
    std::cout << "max_energy: " << p.max_energy << ", max_energy_cost: " << max_energy_cost << std::endl;
    std::cout << "max_missiles: " << p.max_missiles << ", max_missiles_cost: " << max_missiles_cost << std::endl;
    std::cout << "max_flight_speed: " << p.max_flight_speed << ", max_flight_speed_cost: " << max_flight_speed_cost
              << std::endl;
    std::cout << "max_flight_control: " << (p.max_flight_pitch * 2 + p.max_flight_roll)
              << ", flight_control_cost: " << flight_control_cost << std::endl;

    const double result = mass_cost + cargo_cost + energy_recharge_rate_cost + max_energy_cost + max_missiles_cost +
                          max_flight_speed_cost + flight_control_cost;

    return result;
}

const P cobramk1{
    mass : 52292,
    energy_recharge_rate : 2.5,
    max_energy : 150,
    max_cargo : 70,
    max_missiles : 1,
    max_flight_speed : 260,
    max_flight_pitch : 1.2,
    max_flight_roll : 2.0
};

const P cobramk3{
    mass : 214738,
    energy_recharge_rate : 4.0,
    max_energy : 256,
    max_cargo : 35,
    max_missiles : 4,
    max_flight_speed : 350,
    max_flight_pitch : 1.0,
    max_flight_roll : 2.0
};

const P python{
    mass : 259688,
    energy_recharge_rate : 2.5,
    max_energy : 450,
    max_cargo : 115,
    max_missiles : 2,
    max_flight_speed : 200,
    max_flight_pitch : 0.8,
    max_flight_roll : 2.0
};

const P asp{
    mass : 75882,
    energy_recharge_rate : 4.0,
    max_energy : 350,
    max_cargo : 0,
    max_missiles : 1,
    max_flight_speed : 400,
    max_flight_pitch : 1.0,
    max_flight_roll : 2.0
};

const P adder{
    mass : 16045,
    energy_recharge_rate : 2.0,
    max_energy : 85,
    max_cargo : 2,
    max_missiles : 1,
    max_flight_speed : 240,
    max_flight_pitch : 2.0,
    max_flight_roll : 2.8
};

const P mamba{
    mass : 35807,
    energy_recharge_rate : 3.0,
    max_energy : 240,
    max_cargo : 4,
    max_missiles : 0,
    max_flight_speed : 320,
    max_flight_pitch : 1.4,
    max_flight_roll : 2.1
};

const P boa{
    mass : 181789,
    energy_recharge_rate : 3.0,
    max_energy : 450,
    max_cargo : 70,
    max_missiles : 4,
    max_flight_speed : 240,
    max_flight_pitch : 1.0,
    max_flight_roll : 2.8
};

const P ferdelance{
    mass : 65458,
    energy_recharge_rate : 4.5,
    max_energy : 150,
    max_cargo : 12,
    max_missiles : 2,
    max_flight_speed : 300,
    max_flight_pitch : 1.0,
    max_flight_roll : 3.6
};

const P viperinterceptor{
    mass : 35218,
    energy_recharge_rate : 6.0,
    max_energy : 280,
    max_cargo : 0,
    max_missiles : 3,
    max_flight_speed : 520,
    max_flight_pitch : 2.0,
    max_flight_roll : 4.2
};

const P chrysopelea_mk_i{
    mass : 50914,
    energy_recharge_rate : 4.6,
    max_energy : 200,
    max_cargo : 9,
    max_missiles : 4,
    max_flight_speed : 330,
    max_flight_pitch : 1.35,
    max_flight_roll : 2.5
};

const P anaconda{
    mass : 437687,
    energy_recharge_rate : 6.1,
    max_energy : 500,
    max_cargo : 750,
    max_missiles : 7,
    max_flight_speed : 140,
    max_flight_pitch : 0.4,
    max_flight_roll : 0.8
};

int main()
{
    std::cout << rechne(cobramk3) << " <- cobramk3\n-------------\n" << std::endl;
    std::cout << rechne(python) << " <- python\n-------------\n" << std::endl;
    std::cout << rechne(asp) << " <- asp\n-------------\n" << std::endl;
    std::cout << rechne(adder) << " <- adder\n-------------\n" << std::endl;
    std::cout << rechne(mamba) << " <- mamba\n-------------\n" << std::endl;
    std::cout << rechne(ferdelance) << " <- ferdelance\n-------------\n" << std::endl;
    std::cout << rechne(viperinterceptor) << " <- viperinterceptor\n-------------\n" << std::endl;
    std::cout << rechne(chrysopelea_mk_i) << " <- chrysopelea_mk_i\n-------------\n" << std::endl;
    std::cout << rechne(anaconda) << " <- anaconda\n-------------\n" << std::endl;
    std::cout << rechne(cobramk1) << " <- cobramk1\n-------------\n" << std::endl;
    std::cout << rechne(boa) << " <- boa\n-------------\n" << std::endl;
}