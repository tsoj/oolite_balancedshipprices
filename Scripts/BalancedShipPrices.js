this.name           = "BalancedShipPrices";
this.author         = "Tsoj";
this.copyright      = "(C) 2021 Tsoj";
this.licence        = "CC-NC-BY-SA 4.0";
this.description    = "Balances ship prices based on the ships properties";
this.version        = "1.2";

"use strict";

const hyperspaceWeight = 0.20;
const equipmentWeight = 
[
	{k: "EQ_WEAPON_PULSE_LASER", v: 0.20},
	{k: "EQ_FUEL_INJECTION", v: 0.16},
	{k: "EQ_SHIELD_BOOSTER", v: 0.14},
	{k: "EQ_ECM", v: 0.12},
	{k: "EQ_ADVANCED_COMPASS", v: 0.11},
	{k: "EQ_PASSENGER_BERTH", v: 0.09},
	{k: "EQ_HEAT_SHIELD", v: 0.08},
	{k: "EQ_GAL_DRIVE", v: 0.07},
	{k: "EQ_SCANNER_SHOW_MISSILE_TARGET", v: 0.06},
	{k: "EQ_MULTI_TARGET", v: 0.06},
	{k: "EQ_NAVAL_SHIELD_BOOSTER", v: 0.06},
	{k: "EQ_FUEL_SCOOPS", v: 0.04},
	{k: "EQ_ESCAPE_POD", v: 0.04},
	{k: "EQ_NAVAL_ENERGY_UNIT", v: 0.03},
	{k: "EQ_ENERGY_BOMB", v: 0.03},
	{k: "EQ_DOCK_COMP", v: 0.02}
];
// EQ_ENERGY_UNIT and EQ_CARGO_BAY are included in ship property cost calculation

function costFromParameters(
	shipMass,
	maxCargo, hasHyperspaceMotor,
	energyRechargeRate,	extraEnergyUnitAwardable, maxEnergy,
	maxMissiles,
	maxFlightSpeed,	maxPitch, maxYaw, maxRoll
)
{
	var nonHyperspaceCargoCompensation = 1;
	if (!hasHyperspaceMotor) nonHyperspaceCargoCompensation = 1.5;

	if(extraEnergyUnitAwardable)
	{
		energyRechargeRate *= 1.8;
	}

	/* the basic idea is that diminishing returns are applied to the cost of ship properties.
	Additionally some properties are linked to
	the mass (volume) of the ship. For example, having large amounts of cargo capacity is cheaper if the ship has a big size, than if you are trying
	to cram hundreds of TC into an Adder-sized ship. Or having more missiles is cheaper for a big ship than for a small ship.
	Of course, having a bigger ship is not free, the cost scales linearly with the mass (volume).
	On the other hand, high speed or good maneuverability cost less for a small ship. If you have an Anaconda-sized ship that has the maneuverability
	of a Viper you'll have to pay a high price.
	*/
	const massCost = 0.5 * Math.max(0.0, shipMass);
    const cargoCost = 0.5 * Math.max(0.0, 20000.0 * (Math.exp(10000.0 * maxCargo / (3 * shipMass * nonHyperspaceCargoCompensation)) - 1.0));
    const energyRechargeRateCost = 0.5 * Math.max(0.0, 2000.0 * Math.exp(energyRechargeRate / 1.53) / 3.0 - 630.0);
	const maxEnergyCost = 0.5 * Math.max(0.0, 80.0 * (125.0 * Math.exp(maxEnergy/ 200.0) - 112.0));
    const maxMissilesCost = 0.5 * Math.max(0.0, 10.0*(1000.0*Math.exp(500.0*maxMissiles/(19*(Math.pow(shipMass, 1.0/3.0)))) - 991.0));
    const maxFlightSpeedCost = 0.5 * Math.max(0.0, shipMass * Math.exp(maxFlightSpeed/ 80.0) / 70.0);
	const flightControlCost = 0.5 * Math.max(0.0, shipMass * Math.exp(3.0 * (maxPitch + maxYaw + maxRoll) / 4.0) / 80.0);

	if(extraEnergyUnitAwardable)
	{
		const costExtraEnergyUnit = EquipmentInfo.infoForKey("EQ_ENERGY_UNIT").calculatedPrice/10.0;
		energyRechargeRateCost = Math.max(0.0, energyRechargeRateCost - costExtraEnergyUnit);
		// subtracting cost, because extra energy unit might not yet be installed, we calculate cost only for the potential
	}

    log("BalancedShipPrices", "mass: " + shipMass.toFixed(1) + ", massCost: " + massCost.toFixed(1));
    log("BalancedShipPrices", "maxCargo: " + maxCargo.toFixed(1) + ", cargoCost: " + cargoCost.toFixed(1));
    log("BalancedShipPrices", "energyRechargeRate: " + energyRechargeRate.toFixed(1) + ", energyRechargeRateCost: " + energyRechargeRateCost.toFixed(1));
    log("BalancedShipPrices", "maxEnergy: " + maxEnergy.toFixed(1) + ", maxEnergyCost: " + maxEnergyCost.toFixed(1));
    log("BalancedShipPrices", "maxMissiles: " + maxMissiles.toFixed(1) + ", maxMissilesCost: " + maxMissilesCost.toFixed(1));
	log("BalancedShipPrices", "maxSpeed: " + maxFlightSpeed.toFixed(1) + ", maxFlightSpeedCost: " + maxFlightSpeedCost.toFixed(1));
    log("BalancedShipPrices", "pitch/roll/yaw: " + maxPitch.toFixed(1) + "/" +maxRoll.toFixed(1)  + "/" + maxYaw.toFixed(1) + ", flightControlCost: " + flightControlCost.toFixed(1));

    return massCost + cargoCost + energyRechargeRateCost + maxEnergyCost + maxMissilesCost +
						  maxFlightSpeedCost + flightControlCost;
}

function awardable(shipPlist, equipKey)
{
	if (shipPlist._oo_shipyard)
	{
		if (shipPlist._oo_shipyard.standard_equipment.extras && shipPlist._oo_shipyard.standard_equipment.extras.indexOf(equipKey) >= 0)
			return "Standard";
		if (shipPlist._oo_shipyard.optional_equipment && shipPlist._oo_shipyard.optional_equipment.indexOf(equipKey) >= 0)
			return "Optional";
	}
	if(EquipmentInfo.infoForKey(equipKey) == null)
		log("BalancedShipPrices", "Can't find info for key: " + equipKey);
	else if(EquipmentInfo.infoForKey(equipKey).isAvailableToAll)
		return "Optional";
	
	return "N/A";
}

function calculateCost(shipdataKey)
{
	var shipPlist = Ship.shipDataForKey(shipdataKey);
	var ships = system.addShips("["+shipdataKey+"]", 1, Vector3D(100000000, 0, 0));
	if (!ships || ships.length == 0)
	{
		log("BalancedShipPrices", "Can't add ship. Skipping "+ shipdataKey);
		return;
	}
	var ship = ships[0];

	const mass = ship.mass
	const hasHyperspaceMotor = ship.hasHyperspaceMotor
	
	var maxCargo = Number(shipPlist.max_cargo);
	if(awardable(shipPlist, "EQ_CARGO_BAY") != "N/A") maxCargo += ship.extraCargo;
	if(maxCargo != maxCargo) maxCargo = 0;

	for(i in ships)
	{
		ships[i].remove(true);
	}

	var maxMissiles = Number(shipPlist.max_missiles);
	if(maxMissiles != maxMissiles) maxMissiles = 0;

	var maxRoll = Number(shipPlist.max_flight_roll);
	if(maxRoll != maxRoll) maxRoll = 0;
	
	var maxPitch = Number(shipPlist.max_flight_pitch);
	if(maxPitch != maxPitch) maxPitch = 0;

	var maxYaw = Number(shipPlist.max_flight_yaw);
	if(maxYaw != maxYaw) maxYaw = maxPitch;	

	var maxEnergy = Number(shipPlist.max_energy);
	if(maxEnergy != maxEnergy) maxEnergy = 0;

	var energyRechargeRate = Number(shipPlist.energy_recharge_rate);
	if(energyRechargeRate != energyRechargeRate) energyRechargeRate = 0;

	var maxFlightSpeed = Number(shipPlist.max_flight_speed);
	if(maxFlightSpeed != maxFlightSpeed) maxFlightSpeed = 0;
	

	var result = costFromParameters(
		mass ,
		maxCargo, hasHyperspaceMotor,
		energyRechargeRate, (awardable(shipPlist, "EQ_ENERGY_UNIT") != "N/A"), maxEnergy,
		maxMissiles,
		maxFlightSpeed, maxPitch, maxYaw, maxRoll
	);

	if (!hasHyperspaceMotor)
	{
		result *= (1.0 - hyperspaceWeight);
		log("BalancedShipPrices", "Reducing value by "+ (1.0 - hyperspaceWeight).toFixed(2) + " because of missing hyperspace capability");
	}
						  
	for(var i = 0; i<equipmentWeight.length; i++)
	{
		if(awardable(shipPlist, equipmentWeight[i].k) == "N/A")
		{
			result *= (1.0 - equipmentWeight[i].v);
			log("BalancedShipPrices", "Reducing value by "+ (1.0 - equipmentWeight[i].v).toFixed(2) + " because of unawardable " + equipmentWeight[i].k);
		}
	}
	
	result = Math.ceil(result/100)*100;
	
	if(shipPlist._oo_shipyard && shipPlist._oo_shipyard.standard_equipment)
	{
		const w = [
			shipPlist._oo_shipyard.standard_equipment.forward_weapon_type,
			shipPlist._oo_shipyard.standard_equipment.aft_weapon_type,
			shipPlist._oo_shipyard.standard_equipment.port_weapon_type,
			shipPlist._oo_shipyard.standard_equipment.starboard_weapon_type
		];			
		if(shipPlist._oo_shipyard.standard_equipment.extras) w.push.apply(w, shipPlist._oo_shipyard.standard_equipment.extras);

		for(let i in w)
		{
			if(w[i])
			{
				var value =EquipmentInfo.infoForKey(w[i]).calculatedPrice/10.0;
				result += value;
				log("BalancedShipPrices", "Adding value for "+ w[i] + " (standard): " + value);
			}
		}
	}

	log("BalancedShipPrices", "Final value: " + result);

    return result;
}

this.startUp = function()
{
	var dataKeysArray = Ship.keys();
	for(let i in dataKeysArray)
	{
		var shipdata = Ship.shipDataForKey(dataKeysArray[i]);
		if(shipdata._oo_shipyard && shipdata._oo_shipyard.price)
		{
			log("BalancedShipPrices", dataKeysArray[i]);
			var startTime = new Date().getTime();
			shipdata._oo_shipyard.price = calculateCost(dataKeysArray[i]);
			var endTime = new Date().getTime();
			log("BalancedShipPrices", "Time: " + (endTime - startTime));
			log("BalancedShipPrices", "--------------------------------");
		}
		Ship.setShipDataForKey(dataKeysArray[i], shipdata);		
	}
}




















